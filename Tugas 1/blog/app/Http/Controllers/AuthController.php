<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Request as HttpFoundationRequest;

class AuthController extends Controller
{
    public function index()
    {
        return view ('register');
    }

    public function register(Request $request)
    {
        $nama_dpn = $request->input('nama_dpn');
        $nama_blkng= $request->input('nama_blkng');
        $tgl_lahir= $request->input('tgl_lahir');
        $jenis_kelamin= $request->input('jenis_kelamin');

        return redirect ('/welcome')
        ->with('nama_dpn', $nama_dpn)
        ->with('nama_blkng', $nama_blkng)
        ->with('tgl_lahir', $tgl_lahir)
        ->with('jenis_kelamin', $jenis_kelamin);
    }

    public function welcome(Request $request)
    {
        $nama_dpn = $request->session()->get('nama_dpn');
        $nama_blkng= $request->session()->get('nama_blkng');
        $tgl_lahir= $request->session()->get('tgl_lahir');
        $jenis_kelamin= $request->session()->get('jenis_kelamin');

        return view ('welcome',[
        'nama_dpn'=> $nama_dpn,
        'nama_blkng'=> $nama_blkng,
        'tgl_lahir'=> $tgl_lahir,
        'jenis_kelamin'=> $jenis_kelamin,
    ]);
    }
}

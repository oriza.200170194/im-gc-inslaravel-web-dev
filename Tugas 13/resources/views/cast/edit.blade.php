@extends('layout')

@section('isikonten')
<div style="padding: 10px">
    <h1>Edit Data Pemain Film</h1>
@foreach($cast as $cas)
    <form action="/cast/{{ $cas->id }}/update" method="post">
        @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" value="{{ $cas->nama }}" placeholder="Masukkan Nama">
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" class="form-control" id="umur" name="umur" value="{{ $cas->umur }}" placeholder="Masukkan Umur">
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio" value="{{ $cas->bio }}" placeholder="Masukkan Bio">
        </div>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
@endforeach
</div>
@endsection

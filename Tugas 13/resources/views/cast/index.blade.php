@extends('layout')

@section('isikonten')
<div style="padding: 10px">
    <h1>List Data Pemain Film</h1>

    <a href="/cast/create" style="width: 100px; margin-left: 1100px" class="btn btn-primary">Tambah</a>

    <table class="table">
        <thead>
            <tr>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach($cast as $cast)
                <tr>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>
                        <a href="/cast/{{ $cast->id }}" class="btn btn-primary">Detail</a>
                        <a href="/cast/{{ $cast->id }}/edit" class="btn btn-secondary">Edit</a>
                        <form action="/cast/{{ $cast->id }}" method="POST" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger" onclick="return confirm('Apakah Anda yakin ingin menghapus?')">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

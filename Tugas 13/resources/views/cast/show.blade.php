@extends('layout')

@section('isikonten')
    <h1>Detail Pemain Film</h1>

    <table class="table">
        <tr>
            <th>Nama</th>
            <td>{{ $cast->nama }}</td>
        </tr>
        <tr>
            <th>Umur</th>
            <td>{{ $cast->umur }}</td>
        </tr>
        <tr>
            <th>Bio</th>
            <td>{{ $cast->bio }}</td>
        </tr>
    </table>

    <a href="/cast" style="width: 100px; " class="btn btn-primary">Kembali</a> <!-- Tombol Kembali -->
@endsection

<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = Cast::all();
        return view('cast.index', ['cast' => $cast]);
    }

    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        // Validasi data yang dikirim oleh form
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',

        ]);

        // Simpan data ke dalam tabel Cast
        Cast::create($validatedData);

        // Redirect ke halaman index cast
        return redirect('/cast');
    }

    public function show($cast_id)
    {
        $cast = Cast::find($cast_id);
        return view('cast.show', ['cast' => $cast]);
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')
        ->where('id',$cast_id)
        ->get();
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $cast_id)
    {
        // Validasi data yang dikirim oleh form
        $validatedData = $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // Cari data cast yang akan diupdate
        $cast = Cast::find($cast_id);

        // Update data cast
        $cast->update($validatedData);

        // Redirect ke halaman show cast
        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        // Cari data cast yang akan dihapus
        $cast = Cast::find($cast_id);

        // Hapus data cast
        $cast->delete();

        // Redirect ke halaman index cast
        return redirect('/cast');
    }
}
